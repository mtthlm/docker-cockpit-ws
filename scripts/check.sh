if [ ! -d /host/etc -o ! -d /host/proc -o ! -d /host/var/run ]; then
    echo "$0"': host file system is not mounted at /host' >&2
    exit 1
fi

if [ ! -f /host/usr/bin/cockpit-bridge ]; then
    echo "$0"': cockpit-bridge must be installed in the host' >&2
    exit 1
fi

if [ ! -d /host/usr/share/cockpit ]; then
    echo "$0"': cockpit-system and other resources must be installed in the host' >&2
    exit 1
fi

if [ -f /host/usr/libexec/cockpit-ws ]; then
    echo "$0"': cockpit-ws must not be installed in the host' >&2
    exit 1
fi
