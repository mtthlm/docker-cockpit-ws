FROM centos:centos7
MAINTAINER "Matt Helm" <matt.helm@threatq.com>

ADD ./scripts /scripts

RUN yum -y update  --setopt=tsflags=nodocs \
 && yum -y install --setopt=tsflags=nodocs \
        cockpit-ws \
        cockpit-dashboard \
 && yum clean all \
 && chmod +x /scripts/install /scripts/run /scripts/uninstall

CMD ["/scripts/run"]
